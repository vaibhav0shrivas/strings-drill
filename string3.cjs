// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.



function parseIP(passedString){
    let result = "";
    const monthsName = ["January","February","March","April","May","June","July",
    "August","September","October","November","December"];
    if(passedString && typeof passedString === 'string'){
        passedString = passedString.trim();
        if((/^([1-9]|[1-2][0-9]|[30-31]|0[1-9])\/(1[1-2]|[1-9]|0[1-9])\/[0-9]\d*$/).test(passedString) == true){
          
          let month = Number(passedString.split("/")[1]);
          result = monthsName[month - 1];
        }
        
    }
    return result;


}




module.exports = parseIP;