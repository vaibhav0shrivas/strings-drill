const testGetFullName = require('../string4.cjs');

const result0
=testGetFullName();

const result1
=testGetFullName({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"});

const result2
=testGetFullName({"first_name": "JoHN", "last_name": "SMith"});

const result3
=testGetFullName({"first_name": "JoHN", "middle_name": "doe"});

const result4
=testGetFullName("ssddad");

const result5
=testGetFullName({"first_name": "   JoHN", "middle_name": " doe ", "last_name": "SMith  "});

const result6
=testGetFullName({"first_name": 1, "middle_name": "doe", "last_name": "SMith"});

const result7
=testGetFullName({"first_name": "JoHN", "middle_name": 2, "last_name": "SMith"});

const result8
=testGetFullName({"first_name": "JoHN", "middle_name": {a:2}, "last_name": "SMith"});

const result9
=testGetFullName({"first_name": true, "middle_name": "doe", "last_name": "SMith"});

const result10
=testGetFullName({});

console.log(typeof result0,"==>",result0);
console.log(typeof result1,"==>",result1);
console.log(typeof result2,"==>",result2);
console.log(typeof result3,"==>",result3);
console.log(typeof result4,"==>",result4);
console.log(typeof result5,"==>",result5);
console.log(typeof result6,"==>",result6);
console.log(typeof result7,"==>",result7);
console.log(typeof result8,"==>",result8);
console.log(typeof result9,"==>",result9);
console.log(typeof result10,"==>",result10);
// console.log(typeof result5,"==>",result5);
// console.log(typeof result5,"==>",result5);
