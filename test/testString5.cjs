const testArrayToString = require('../string5.cjs');

const result0
=testArrayToString();

const result1
=testArrayToString(["the", "quick", "brown", "fox"]);

const result2
=testArrayToString(["the", "  quick", "  brown ", "fox  "]);

const result3
=testArrayToString(["the", "quick", 3, "fox"]);

const result4
=testArrayToString("ssddad");

const result5
=testArrayToString({"first_name": "   JoHN", "middle_name": " doe ", "last_name": "SMith  "});

const result6
=testArrayToString(6);

const result7
=testArrayToString(["this", "is", ["nested", "array"]]);

const result8
=testArrayToString(["this", "is", ["nested", "array"], "sentence"]);

const result9
=testArrayToString(["the", "quick", "brown", "fox!"]);

const result10
=testArrayToString({});

console.log(typeof result0,"==>",result0);
console.log(typeof result1,"==>",result1);
console.log(typeof result2,"==>",result2);
console.log(typeof result3,"==>",result3);
console.log(typeof result4,"==>",result4);
console.log(typeof result5,"==>",result5);
console.log(typeof result6,"==>",result6);
console.log(typeof result7,"==>",result7);
console.log(typeof result8,"==>",result8);
console.log(typeof result9,"==>",result9);
console.log(typeof result10,"==>",result10);
// console.log(typeof result5,"==>",result5);
// console.log(typeof result5,"==>",result5);
