const testParseIP = require('../string3.cjs');

const result0
=testParseIP();

const result1
=testParseIP("11/12/1634");

const result2
=testParseIP("01/22/45");

const result3
=testParseIP("35/12/1998");

const result4
=testParseIP("14:12:2022");

const result5
=testParseIP("25/13.4/19");

const result6
=testParseIP("y/y/y/y");

const result7
=testParseIP("  35/12/1998");

const result8
=testParseIP("   3/12/1998");

const result9
=testParseIP(" 02/5/1234 ");

const result10
=testParseIP("1/03/8945 ");

console.log(typeof result0,"==>",result0);
console.log(typeof result1,"==>",result1);
console.log(typeof result2,"==>",result2);
console.log(typeof result3,"==>",result3);
console.log(typeof result4,"==>",result4);
console.log(typeof result5,"==>",result5);
console.log(typeof result6,"==>",result6);
console.log(typeof result7,"==>",result7);
console.log(typeof result8,"==>",result8);
console.log(typeof result9,"==>",result9);
console.log(typeof result10,"==>",result10);
// console.log(typeof result5,"==>",result5);
// console.log(typeof result5,"==>",result5);
