const testGetNumber = require('../string1.cjs');

const result0
=testGetNumber();

const result1
=testGetNumber("$100.45");

const result2
=testGetNumber("$1,002.22");

const result3
=testGetNumber("$0");

const result4
=testGetNumber("12");

const result5
=testGetNumber("-$123");

const result6
=testGetNumber("-$12a3");

const result7
=testGetNumber("-$12   3 ");

const result8
=testGetNumber("  -$1236     ");

const result9
=testGetNumber();

const result10
=testGetNumber(45);

console.log(typeof result0,"==>",result0);
console.log(typeof result1,"==>",result1);
console.log(typeof result2,"==>",result2);
console.log(typeof result3,"==>",result3);
console.log(typeof result4,"==>",result4);
console.log(typeof result5,"==>",result5);
console.log(typeof result6,"==>",result6);
console.log(typeof result7,"==>",result7);
console.log(typeof result8,"==>",result8);
console.log(typeof result9,"==>",result9);
console.log(typeof result10,"==>",result10);
// console.log(typeof result5,"==>",result5);
// console.log(typeof result5,"==>",result5);
