const testParseIP = require('../string2.cjs');

const result0
=testParseIP();

const result1
=testParseIP("115.42.150.37");

const result2
=testParseIP("192.168.0.1");

const result3
=testParseIP("110.234.52.124");

const result4
=testParseIP("210.110");

const result5
=testParseIP("255");

const result6
=testParseIP("y.y.y.y");

const result7
=testParseIP("255.0.0.y");

const result8
=testParseIP("666.10.10.20");

const result9
=testParseIP("4444.11.11.11");

const result10
=testParseIP(" 152.164.85.24  ");

console.log(typeof result0,"==>",result0);
console.log(typeof result1,"==>",result1);
console.log(typeof result2,"==>",result2);
console.log(typeof result3,"==>",result3);
console.log(typeof result4,"==>",result4);
console.log(typeof result5,"==>",result5);
console.log(typeof result6,"==>",result6);
console.log(typeof result7,"==>",result7);
console.log(typeof result8,"==>",result8);
console.log(typeof result9,"==>",result9);
console.log(typeof result10,"==>",result10);
// console.log(typeof result5,"==>",result5);
// console.log(typeof result5,"==>",result5);
