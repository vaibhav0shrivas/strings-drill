// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}


function getFullName(passedObject){
    let fullName = "",smallcase="";
    if(passedObject && typeof passedObject === 'object'){
        if(passedObject.first_name && typeof passedObject.first_name === 'string' ){
           smallcase = passedObject.first_name.trim().toLowerCase();
           smallcase = smallcase[0].toUpperCase() + smallcase.slice(1);
           fullName = fullName + smallcase;

           if(passedObject.middle_name && typeof passedObject.middle_name === 'string'){
            smallcase = passedObject.middle_name.trim().toLowerCase();
            smallcase = smallcase[0].toUpperCase() + smallcase.slice(1);
            fullName = fullName.concat(" ",smallcase);

           }

           if(passedObject.last_name && typeof passedObject.last_name === 'string'){
            smallcase = passedObject.last_name.trim().toLowerCase();
            smallcase = smallcase[0].toUpperCase() + smallcase.slice(1);
            fullName = fullName.concat(" ",smallcase);

           }


        }
    }
    return fullName;


}




module.exports = getFullName;