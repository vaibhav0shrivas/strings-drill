// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

function arrayToString(passedArray){
    let resultString="",words = [];
    if(passedArray && Array.isArray(passedArray)){

        passedArray.forEach(function(element){
            if(typeof element === 'string'){
                words.push(element.trim());
            }
        });
        resultString = words.join(" ");
        if(resultString.endsWith(".") == false && resultString.endsWith("?") == false && resultString.endsWith("!") == false){
            resultString = resultString + ".";
        }
    }
    return resultString;

}

module.exports = arrayToString;