// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and
//  return it in an array in numeric values. [111, 139, 161, 143].
// Support IPV4 addresses only. If there are other characters detected, return an empty array.


function ValidateIPaddress(ipaddress) {  
    if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {  
      return (true)  
    }  

    return (false)  
}  
function parseIP(passedString){
    let result = [];
    
    if(passedString && typeof passedString === 'string'){
        passedString = passedString.trim();
        if(ValidateIPaddress(passedString) == true){
            result = passedString.split('.');
        }
        
    }
    return result;


}




module.exports = parseIP;